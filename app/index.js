const atlas = require('@rs-components/atlas');

atlas({
    application: {
        name: 'TestApp_Yana',
        features: {
            subdomain: false,
        },
    },
    environment: {
        AGGREGATION_HOST: process.env.AGGREGATION_HOST,
        PORT: 3000,
        NODE_ENV: process.env.NODE_ENV,
    },
    routes: [ ]
});