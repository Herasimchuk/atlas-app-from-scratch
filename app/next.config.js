const withTM = require('next-transpile-modules');

module.exports = withTM({
  distDir: 'build',
  transpileModules: [
    '@rs-components/atlas-apollo',
  ],
});
