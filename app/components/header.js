import Link from 'next/link';

const Header = () => (
    <div>
        <p>
            <Link href='/'>
                <a style={{marginRight: '10px'}}>
                    Home (test)
                </a>
            </Link>   
            <Link href='/users'>
                <a>
                    Users
                </a>
            </Link>
        </p>
    </div>
)

export default Header;