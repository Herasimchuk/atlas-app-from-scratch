import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Header from '../components/header';

const allUsersQuery = gql`
    query {
        allUsers(first: 5) {
            firstName,
            lastName
        }
    }
`;

const Users = props => {
    const { data } = useQuery(allUsersQuery);
    return (
        <>
            <Header />
            <div>
                All users
            <ul>
                    {data && data.allUsers.map(({ firstName, lastName }) => (<li>{firstName} {lastName}</li>))}
                </ul>
            </div>
        </>
    )
}

export default Users;